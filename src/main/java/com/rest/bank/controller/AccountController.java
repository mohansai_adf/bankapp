package com.rest.bank.controller;

import com.rest.bank.entity.Account;
import com.rest.bank.entity.Transaction;
import com.rest.bank.service.BankAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;


@RestController
public class AccountController {
    /* default */ DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    /* default */ LocalDateTime now = LocalDateTime.now();
    @Autowired
    /* default */ BankAccountService service;
    @RequestMapping(value="/bank",method=RequestMethod.POST)
    public Account addAccount(@RequestBody InputData data){
        Account acc = new Account();
        acc.setAccountType(data.getAccountType());
        if (Objects.equals(data.getAccountType(), "CURRENT")){
            acc.setTransactionFee(5);
        }else{
            acc.setTransactionFee(0);
        }
        acc.setName(data.getName());
        String dob = data.getDob();
        dob += " 00:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateOfBirth = LocalDateTime.parse(dob, formatter);
        acc.setDob(dateOfBirth);
        acc.setCreatedAt(now);
        acc.setUpdatedAt(now);
        acc.setBalance(0);
        service.addAccount(acc);
        if (data.getInitialDeposit() > 0){
            operations(data.getInitialDeposit(), "DEPOSIT", acc.getAccountNumber());
        }
        return acc;
    }

    @RequestMapping(value="/bank", method = RequestMethod.GET)
    public Transaction operations(@RequestParam int amount,
                                  @RequestParam String type,
                                  @RequestParam int accountNumber){
        Transaction tran = new Transaction();
        Account acc = service.getDetails(accountNumber);
        String status = "FAILURE";
        int balance = acc.getBalance();
        int transactionFee = acc.getTransactionFee();
        acc.setUpdatedAt(now);
        if (Objects.equals(type, "DEPOSIT")) {
            balance += amount;
            balance -= transactionFee;
            status = "SUCCESS";
            tran.setTransactionType("DEPOSIT");
        }else{
            if (balance >= amount) {
                balance += amount;
                acc.setBalance(balance);
                acc.setUpdatedAt(now);
                status = "SUCCESS";
                tran.setTransactionType("WITHDRAWL");
            }
        }
        acc.setBalance(balance);
        tran.setAccountNumber(accountNumber);
        tran.setAmount(amount);
        tran.setStatus(status);
        tran.setCreatedAt(now);
        tran.setUpdatedAt(now);
        dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        now = LocalDateTime.now();
        tran.setTransactionDate(now);
        return service.addTransaction(tran);
    }

    @RequestMapping(value="/bank/history", method = RequestMethod.GET)
    public TransactionHistory transactions(@RequestParam int accountNumber,
                                           @RequestParam String startDate,
                                           @RequestParam String endDate){
        startDate += " 00:00";
        endDate += " 00:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime start = LocalDateTime.parse(startDate, formatter);
        LocalDateTime end = LocalDateTime.parse(endDate, formatter);
        Account acc = service.getDetails(accountNumber);
        TransactionHistory history = new TransactionHistory();
        history.setAccountNumber(acc.getAccountNumber());
        history.setName(acc.getName());
        history.setAccountType(acc.getAccountType());
        history.setBalance(acc.getBalance());
        history.setTransactionFee(acc.getTransactionFee());
        history.setCreatedAt(acc.getCreatedAt());
        history.setUpdatedAt(acc.getUpdatedAt());
        history.setAllTransactions(service.getTransactions(accountNumber, start, end));
        return history;
    }
}
