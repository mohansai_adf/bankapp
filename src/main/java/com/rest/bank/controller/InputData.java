package com.rest.bank.controller;

import javax.persistence.Column;

public class InputData {
    private String name;
    private String dob;
    @Column(columnDefinition = "VARCHAR(60) CHECK (account_type IN ('SAVINGS','CURRENT'))")
    private String accountType;
    private int initialDeposit;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public int getInitialDeposit() {
        return initialDeposit;
    }

    public void setInitialDeposit(int initialDeposit) {
        this.initialDeposit = initialDeposit;
    }
}
