package com.rest.bank.repository;

import com.rest.bank.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.time.LocalDateTime;
import java.util.List;

public interface TransactionRepository extends JpaRepository<Transaction, Integer> {
    @Query(value = "Select * from transaction where account_number=?1 and transaction_date between?2 and ?3", nativeQuery = true)
    List<Transaction> getTransactions(int accountNumber, LocalDateTime startDate, LocalDateTime endDate);
}
