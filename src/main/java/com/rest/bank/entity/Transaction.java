package com.rest.bank.entity;

import lombok.*;
import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Transaction")
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer transactionId;
    private Integer accountNumber;
    private int amount;
    private String transactionType;
    private LocalDateTime transactionDate;
    private String status;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
}
