package com.rest.bank.service;

import com.rest.bank.entity.Account;
import com.rest.bank.entity.Transaction;
import com.rest.bank.repository.BankAccountRepository;
import com.rest.bank.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class BankAccountService{
    @Autowired
    private BankAccountRepository repository;
    @Autowired
    private TransactionRepository repository2;

    public Account addAccount(@Valid Account account){
        return repository.save(account);
    }
    public Transaction addTransaction(Transaction tran) {
        return repository2.save(tran);
    }
    public Account getDetails(int accountNumber){
        return repository.getData(accountNumber);
    }
    public List<Transaction> getTransactions(int accountNumber, LocalDateTime startDate, LocalDateTime endDate){return repository2.getTransactions(accountNumber, startDate, endDate);}
}