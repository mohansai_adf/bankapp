package com.rest.bank.repository;

import com.rest.bank.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BankAccountRepository extends JpaRepository<Account, Integer> {
    @Query(value = "Select * from bank_account acc where acc.account_number=?1", nativeQuery = true)
    Account getData(int accNumber);
}
